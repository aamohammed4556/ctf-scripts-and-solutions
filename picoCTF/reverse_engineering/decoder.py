"""Solution to Transformation picoCTF challenge"""
flag = '灩捯䍔䙻ㄶ形楴獟楮獴㌴摟潦弸彥ㄴㅡて㝽'
decrypt = ''
for i in range(len(flag)):
    decrypt += chr(ord(flag[i])>>8)
    decrypt += chr((ord(flag[i]))-((ord(flag[i])>>8)<<8))

print(decrypt)
