
all_possible_pwds = []

with open('test_pw.txt', 'w') as pw_list:
    for a in range(29,30):
        if a < 10:
            a = '0' + str(a)
        for b in range(1, 56):
            if b < 10:
                b = '0' + str(b)
            for c in range(1, 56 ):
                if c < 10:
                    c = '0' + str(c)
                pw = '-'.join([str(a), str(b), str(c)])
                print(pw)
                all_possible_pwds.append(pw)
                pw_list.write(pw.strip() + '\n')

print(len(all_possible_pwds))